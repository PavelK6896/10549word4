import React, {useMemo, useState} from "react";
import {Slice, Word} from "./models";

export const WORDS = "http://localhost:8080/api/words"

export const GET = async (path: string, params?: any) => {
    const url = new URL(path)
    url.search = new URLSearchParams({...params}).toString()
    const response = await fetch(url.toString())
    return await response.json() as Slice<Word>
}

export const getWords = async (param: any) => {
    const {content, number, last} = await GET(WORDS, param)
    return {content, number, last}
}

export interface WordsState {
    page: number
    last: boolean
    loading: boolean
}

export function HomePage() {

    const [words, setWords] = useState<Word[]>([{id: "", translate1: "", word1: ""}]);
    const [state, setState] = useState<WordsState>({loading: false, page: 0, last: false});

    const getW = () => {
        console.log('getW')
        setState(prevState => ({...prevState, loading: true}))
        getWords({page: state.page + 1})
            .then(f => {
                setWords(words => [...words, ...f.content])
                setTimeout(() => {
                    setState(prevState => ({...prevState, loading: false, page: state.page + 1, last: f.last}))
                }, 2000)
            })
    }

    useMemo(() => {
        console.log('useMemo')
        setState(prevState => ({...prevState, loading: true}))
        getWords({page: state.page})
            .then(f => {
                setWords(words => [...words, ...f.content])
                setState(prevState => ({...prevState, loading: false, last: f.last}))
            })
    }, [])

    return (
        <div style={{
            display: "flex",
            flexDirection: "column",
            height: '300px',
            width: '500px',
            overflow: "auto"
        }}
             onScroll={(event) => {
                 if ((event.currentTarget.scrollHeight - event.currentTarget.clientHeight - 100) <= event.currentTarget.scrollTop) {
                     if (!state.loading && !state.last) {
                         getW()
                     }
                 }
             }}
        >
            {words.map((v, k) => {
                return <div key={v.id}>
                    <div style={{display: "flex", flexDirection: "row"}}>
                        <div style={{backgroundColor: '#a5bda5'}}>{v.id}</div>
                        <div style={{backgroundColor: '#406e38'}}>{v.word1}</div>
                        <div style={{backgroundColor: '#92c099'}}>{v.translate1}</div>
                    </div>
                </div>
            })}
            {state.loading && <div className="lds-dual-ring">Loading...</div>}

        </div>
    );
}