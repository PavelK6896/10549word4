
export interface Sort {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
}

export interface Pageable {
    sort: Sort;
    offset: number;
    pageNumber: number;
    pageSize: number;
    unpaged: boolean;
    paged: boolean;
}

export interface Sort2 {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
}

export interface Slice<T> {
    content: T[];
    pageable: Pageable;
    size: number;
    number: number;
    sort: Sort2;
    first: boolean;
    last: boolean;
    numberOfElements: number;
    empty: boolean;
}

export interface Word {
    id: string
    word1: string
    translate1: string
}


