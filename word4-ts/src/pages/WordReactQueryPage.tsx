import React from "react";
import {useInfiniteQuery} from "react-query";
import {Slice, Word} from "./models";

export function WordReactQueryPage() {
    const fetchProjects = async ({pageParam = "0"}) => {
        const url = new URL("http://localhost:8080/api/words")
        let urlSearchParams = new URLSearchParams();
        urlSearchParams.set("page", pageParam)
        url.search = urlSearchParams.toString()
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(fetch(url).then((res) => res.json()).then(f => f as Slice<Word>));
            }, 2000)
        }).then(f => f as Slice<Word>)
    }

    const {
        data,
        error,
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,
        status,
    } = useInfiniteQuery({
        queryKey: ['projects'],
        queryFn: fetchProjects,
        getNextPageParam: (lastPage, pages) => lastPage.number + 1,
    })

    return <div style={{
        display: "flex",
        flexDirection: "column",
        height: '300px',
        width: '500px',
        overflow: "auto"
    }}
                onScroll={(event) => {
                    if ((event.currentTarget.scrollHeight - event.currentTarget.clientHeight - 100) <= event.currentTarget.scrollTop) {
                        if (!(!hasNextPage || isFetchingNextPage)) {
                            fetchNextPage()
                        }
                    }
                }}
    >
        {data?.pages.map((group, i) => (
            <React.Fragment key={i}>
                {group.content.map(v => (
                    <div key={v.id}>
                        <div style={{display: "flex", flexDirection: "row"}}>
                            <div style={{backgroundColor: '#a5bda5'}}>{v.id}</div>
                            <div style={{backgroundColor: '#406e38'}}>{v.word1}</div>
                            <div style={{backgroundColor: '#92c099'}}>{v.translate1}</div>
                        </div>
                    </div>
                ))}
            </React.Fragment>
        ))}
        {(status === 'loading' || isFetching) && <div className="lds-dual-ring">Fetching...</div>}
        {status === 'error' && <React.Fragment>Error {JSON.stringify(error)}</React.Fragment>}
    </div>
}
