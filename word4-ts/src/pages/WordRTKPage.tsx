import React, {useMemo, useState} from "react";
import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {Slice, Word} from "./models";
import {WordsState} from "./HomePage";

export function WordRTKPage() {

    const [state, setState] = useState<WordsState>({loading: false, page: 0, last: false});
    const [word, setWord] = useState<Word[]>([]);
    const {data} = usePageQuery(state.page, {skip: state.page < 0})

    useMemo(() => {
        console.log('useMemo')
        if (data) {
            setWord(state => ([...state, ...data.content]))
            setState(state => ({...state, last: data.last, loading: false}))
        }
    }, [data])

    return (
        <div style={{
            display: "flex",
            flexDirection: "column",
            height: '300px',
            width: '500px',
            overflow: "auto"
        }}
             onScroll={(event) => {
                 if ((event.currentTarget.scrollHeight - event.currentTarget.clientHeight - 100) <= event.currentTarget.scrollTop) {
                     if (!state.loading && !state.last) {
                         setState(state => ({...state, loading: true}))
                         setTimeout(() => {
                             setState(state => ({...state, page: state.page + 1}))
                         }, 2000)
                     }
                 }
             }}
        >
            {word.map((v) => {
                return <div key={v.id}>
                    <div style={{display: "flex", flexDirection: "row"}}>
                        <div style={{backgroundColor: '#a5bda5'}}>{v.id}</div>
                        <div style={{backgroundColor: '#406e38'}}>{v.word1}</div>
                        <div style={{backgroundColor: '#92c099'}}>{v.translate1}</div>
                    </div>
                </div>
            })}
            {state.loading && <div className="lds-dual-ring">Loading...</div>}
        </div>
    );
}

export const wordsApi = createApi({
    reducerPath: 'wordsApi/api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8080/'
    }),
    endpoints: build => ({
        page: build.query<Slice<Word>, number>({
            query: (page: number) => ({
                url: 'api/words',
                params: {
                    page
                }
            })
        })
    })

})

export const {usePageQuery} = wordsApi


