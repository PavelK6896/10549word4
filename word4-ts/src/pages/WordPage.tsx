import {configureStore, createSlice, PayloadAction} from "@reduxjs/toolkit";
import {Slice, Word} from "./models";
import {TypedUseSelectorHook, useDispatch, useSelector} from "react-redux";
import React, {useMemo} from "react";
import {GET, WORDS} from "./HomePage";
import {setupListeners} from "@reduxjs/toolkit/dist/query/react";
import {wordsApi} from "./WordRTKPage";

export function WordPage() {
    const dispatch = useAppDispatch()
    const slice: WordsSlice = useAppSelector((s) => s.words);

    const extracted = (page: number) => {
        dispatch(load(true))
        GET(WORDS, {page: page})
            .then(f => {
                setTimeout(() => {
                    dispatch(wordsSliceGetSlice(f))
                }, 2000)
            })
    };

    useMemo(() => {
        console.log('useMemo')
        if (!slice.loading && !slice.last && slice.words.length === 0) {
            extracted(0)
        }
    }, [])


    return (
        <div style={{
            display: "flex",
            flexDirection: "column",
            height: '300px',
            width: '500px',
            overflow: "auto"
        }}
             onScroll={(event) => {
                 if ((event.currentTarget.scrollHeight - event.currentTarget.clientHeight - 100) <= event.currentTarget.scrollTop) {
                     if (!slice.loading && !slice.last) {
                         extracted(slice.page + 1)
                     }
                 }
             }}
        >
            {slice.words.map((v) => {
                return <div key={v.id}>
                    <div style={{display: "flex", flexDirection: "row"}}>
                        <div style={{backgroundColor: '#a5bda5'}}>{v.id}</div>
                        <div style={{backgroundColor: '#406e38'}}>{v.word1}</div>
                        <div style={{backgroundColor: '#92c099'}}>{v.translate1}</div>
                    </div>
                </div>
            })}
            {slice.loading && <div className="lds-dual-ring">Loading...</div>}
        </div>
    );
}


export interface WordsSlice {
    words: Word[]
    page: number
    last: boolean
    loading: boolean
}

const words: WordsSlice = {last: false, loading: false, page: 0, words: []}

const wordsSlice = createSlice({
    name: 'words',
    initialState: words,
    reducers: {
        load: (state, action: PayloadAction<boolean>) => {
            if (action.payload) {
                state.loading = true
            }
        },
        wordsSliceGetSlice: (state, action: PayloadAction<Slice<Word>>) => {
            if (action.payload) {
                state.last = action.payload.last
                state.page = action.payload.number
                state.words.push(...action.payload.content)
                state.loading = false
            }
        }
    }
})

export const {wordsSliceGetSlice, load} = wordsSlice.actions
export default wordsSlice.reducer

export const store = configureStore({
    reducer: {
        words: wordsSlice.reducer,
        [wordsApi.reducerPath]: wordsApi.reducer,
    },
    middleware: getDefaultMiddleware => getDefaultMiddleware().concat(wordsApi.middleware)
})
setupListeners(store.dispatch)

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector