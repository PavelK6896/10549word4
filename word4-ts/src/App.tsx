import React from 'react';
import './App.css';
import {HomePage} from "./pages/HomePage";
import {Link, Route, Routes} from "react-router-dom";
import {WordApiPage} from "./pages/WordApiPage";
import {store, WordPage} from "./pages/WordPage";
import {Provider} from "react-redux";
import {WordRTKPage} from "./pages/WordRTKPage";
import {WordReactQueryPage} from "./pages/WordReactQueryPage";
import {QueryClient, QueryClientProvider} from 'react-query'

const queryClient = new QueryClient()

function App() {
    return (
        <div className="">
            <header className="">
             <span>
                 <Link to="/" className="">Home</Link>
                 <Link to="/word" className="">WordPage</Link>
                 <Link to="/word-api" className="">WordApiPage</Link>
                 <Link to="/word-rtk" className="">WordRTKPage</Link>
                 <Link to="/word-rq" className="">WordReactQueryPage</Link>
             </span>
            </header>
            <Routes>
                <Route path="/" element={<HomePage/>}> </Route>
            </Routes>
            <Provider store={store}>
                <QueryClientProvider client={queryClient}>
                    <Routes>
                        <Route path="/word" element={<WordPage/>}> </Route>
                        <Route path="/word-api" element={<WordApiPage/>}> </Route>
                        <Route path="/word-rtk" element={<WordRTKPage/>}> </Route>
                        <Route path="/word-rq" element={<WordReactQueryPage/>}> </Route>
                    </Routes>
                </QueryClientProvider>
            </Provider>
        </div>
    );
}

export default App;
