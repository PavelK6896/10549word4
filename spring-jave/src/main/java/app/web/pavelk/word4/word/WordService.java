package app.web.pavelk.word4.word;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
@RequiredArgsConstructor
public class WordService {

    private final WordRepo wordRepo;
    private final Init init;

    @Value("${initWords2}")
    String initWords2;

    @PostConstruct
    public void initWords2() throws IOException {
        long l = System.nanoTime();
        if (initWords2.equals("true")) {
            init.two();
        }
        log.info(String.valueOf(System.nanoTime() - l));
    }

    public Slice<WordTable> getWords(Integer page) {
        PageRequest of = PageRequest.of(page, 25);
        return wordRepo.findSlice(of);
    }

}
