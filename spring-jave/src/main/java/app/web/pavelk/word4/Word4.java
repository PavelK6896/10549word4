package app.web.pavelk.word4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Word4 {

	public static void main(String[] args) {
		SpringApplication.run(Word4.class, args);
	}

}
