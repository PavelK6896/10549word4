package app.web.pavelk.word4.word;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WordRepo extends JpaRepository<WordTable, Long> {

    @Query(value = "select w from WordTable w")
    Slice<WordTable> findSlice(Pageable pageable);
}
