create table if not exists word (
    id         bigint auto_increment not null,
    word1      varchar,
    translate1 varchar,
    primary key (id)
);



create table if not exists info (
    id        bigint auto_increment not null,
    word      varchar,
    translate varchar,
    primary key (id)
);
